<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Makai\SystemMessagesBundle\Entity\SystemMessages;
use Makai\SystemMessagesBundle\Form\SystemMessagesType;
use Makai\SystemMessagesBundle\Form\SystemMessagesAdminType;

/**
 * SystemMessages controller.
 *
 */
class SystemMessagesController extends Controller
{
    /**
     * Lists all SystemMessages entities.
     *
     */
    public function indexAction()
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
            ->getRepository('MakaiSystemMessagesBundle:SystemMessages')
            ->createQueryBuilder('sm')
            ->orderBy('sm.createdAt', 'DESC');

        $entities = $filterBuilder->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            30/*limit per page*/,
            array('distinct' => false)
        );

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:index.html.twig', array(
            "pagination"=>$pagination,
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a SystemMessages entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MakaiSystemMessagesBundle:SystemMessages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SystemMessages entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        
			));
    }

    /**
     * Displays a form to create a new SystemMessages entity.
     *
     */
    public function newAction()
    {
        $entity = new SystemMessages();
		
		if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ||
			$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')	
		){
			$user = $this->container->get('security.context')->getToken()->getUser();
			//ha belépett felhasználó, akkor kitöltjük az email címet
			if ($user && $user->getId()) {
				$entity->setEmail($user->getEmail());
			}
		}

        $form   = $this->createForm(new SystemMessagesType(), $entity);

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'current'=> "systemmessage"
        ));
    }

    /**
     * Creates a new SystemMessages entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new SystemMessages();
        $form = $this->createForm(new SystemMessagesType(), $entity);
        $form->bind($request);
        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $detectedInfo = $this->getUserInformation()."<br/><br/>".$entity->getDetectedInfo();
                $entity->setDetectedInfo($detectedInfo);
                $em->persist($entity);
                $em->flush();

                $transport = \Swift_MailTransport::newInstance();
                // Create the Mailer using your created Transport
                $mailer = \Swift_Mailer::newInstance($transport);

                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('system_message.message.subject'))
                    ->setFrom($this->container->getParameter('system_message.emails.from_email'))
                    ->setTo($this->container->getParameter('system_message.emails.to_email'))
                    ->setBody($this->renderView('MakaiSystemMessagesBundle:Default:systemMessageEmail.txt.twig', array('entity' => $entity)));
                $mailer->send($message);

                $this->get('session')->getFlashBag()->add('system_message_notice', $this->get('translator')->trans('system_message.message.success'));
                
				/*ez egy csúnya hack, hogy üres legyen a form, mert amikor megpróbáltam redirectelni sikeres mentés után,
                * akkor nem akar törlődni a session flashbag
                */
                $entity = new SystemMessages();
                $form   = $this->createForm(new SystemMessagesType(), $entity);

                /*sikeres mail küldés után átírányítjuk a kapcsolat oldalra, hogy ne ha frissíti az oldalt, akkor
                 * ne küldje el újra az üzenetet
                 */

                //return $this->redirect($this->generateUrl('systemmessages_new'));
                //return $this->redirect($this->generateUrl('systemmessages_show', array('id' => $entity->getId())));
            }
        }

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'current'=> "systemmessage"
        ));
    }

    /**
     * Displays a form to edit an existing SystemMessages entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MakaiSystemMessagesBundle:SystemMessages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SystemMessages entity.');
        }

        $editForm = $this->createForm(new SystemMessagesAdminType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing SystemMessages entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MakaiSystemMessagesBundle:SystemMessages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SystemMessages entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SystemMessagesAdminType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('system_message_notice', $this->get('translator')->trans('save.message.success'));

            return $this->redirect($this->generateUrl('admin_systemmessages_edit', array('id' => $id)));
        }

        return $this->render('MakaiSystemMessagesBundle:SystemMessages:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SystemMessages entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MakaiSystemMessagesBundle:SystemMessages')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SystemMessages entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('systemmessages'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function getUserInformation(){


        $ip = $this->container->get('request')->getClientIp();
        
		$hostaddress = gethostbyaddr($ip);

		$browser = $_SERVER['HTTP_USER_AGENT'];
        
		$referred = $_SERVER['HTTP_REFERER'];

        $result= "IP address:<br />\n";
        $result.= "$ip<br /><br />\n";
        $result.= "Host address:<br />\n";
        $result.= "$hostaddress<br /><br />\n";
        $result.= "Browser info:<br />\n";
        $result.= "$browser<br /><br />\n";

        return $result;
    }
}
