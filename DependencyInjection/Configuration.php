<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('makai_system_messages');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
            ->scalarNode('system_message_emails_to_email')
            ->defaultValue('nash4ever@gmail.com')
            ->info('Erre az email címre küldjünk üzenetet, ha új hibabejelentés érkezik.')
            ->end()
            ->scalarNode('system_message_emails_from_email')
            ->defaultValue('nash4ever@gmail.com')
            ->info('Erről az email címről küldjünk üzenetet, ha új hibabejelentés érkezik.')
            ->end()
            ->end();
        return $treeBuilder;
    }
}
