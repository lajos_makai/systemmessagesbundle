## Telepítés ##

### For Symfony >= 2.3.* ###

A bundle dependecy-be tartozik a SonataIntlBundle:

Fel kell venni a composer.json fájlba:

```
{
    "require": {
        "makai/system-messages-bundle": "dev-master",
    }
     "repositories": [
        {
            "type": "vcs",
            "url": "https://lajos_makai@bitbucket.org/lajos_makai/systemmessagesbundle.git"
        }
    ]
}
```


### Bundle telepítése ###

```
$ php composer.phar update makai/system-messages-bundle
```

### Bundle regisztrálása###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new Makai\SystemMessagesBundle\MakaiSystemMessagesBundle(),
    );
}
```

app/routing.yml fájlba fel kell venni:

```
# MakaiSystemMessagesBundle
#hibabejelentő oldalak
makai_system_messages:
    resource: "@MakaiSystemMessagesBundle/Resources/config/routing.yml"
    prefix:   hu/hibabejelentes

#hibabejelentő admin oldalak
MakaiSystemMessagesBundle_systemmessages:
        resource: "@MakaiSystemMessagesBundle/Resources/config/routing/systemmessages.yml"
        prefix:   admin/hibabejelentes
```

### Bundle paraméterezési lehetőségei ###
```
#hibabejelentő emailt kinek küldjük
system_message.emails.to_email:  nash4ever@gmail.com
#hibabejelentő milyen email címről küldjük
system_message.emails.from_email: nash4ever@gmail.com
```

### Adatbázis schema frissítése ###
```
php app/console doctrine:schema:update --force
```