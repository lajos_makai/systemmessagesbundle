<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Type;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * SystemMessages
 *
 * @ORM\Table(name="system_messages")
 * @ORM\Entity
 */
class SystemMessages
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="instrument", type="string", length=255, nullable=false)
     */
    private $instrument;

    /**
     * @var string
     *
     * @ORM\Column(name="browser", type="string", length=255, nullable=false)
     */
    private $browser;

    /**
     * @var string
     *
     * @ORM\Column(name="operation_system", type="string", length=255, nullable=false)
     */
    private $operationSystem;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_service", type="string", length=255, nullable=false)
     */
    private $internetService;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_speed", type="string", length=255, nullable=false)
     */
    private $internetSpeed;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="detected_info", type="text", nullable=false)
     */
    private $detectedInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_closed", type="boolean", nullable=true)
     */
    private $isClosed;

    /**
     * Set country
     *
     * @param string $country
     * @return SystemMessages
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set instrument
     *
     * @param string $instrument
     * @return SystemMessages
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;
    
        return $this;
    }

    /**
     * Get instrument
     *
     * @return string 
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set browser
     *
     * @param string $browser
     * @return SystemMessages
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;
    
        return $this;
    }

    /**
     * Get browser
     *
     * @return string 
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * Set operationSystem
     *
     * @param string $operationSystem
     * @return SystemMessages
     */
    public function setOperationSystem($operationSystem)
    {
        $this->operationSystem = $operationSystem;
    
        return $this;
    }

    /**
     * Get operationSystem
     *
     * @return string 
     */
    public function getOperationSystem()
    {
        return $this->operationSystem;
    }

    /**
     * Set internetService
     *
     * @param string $internetService
     * @return SystemMessages
     */
    public function setInternetService($internetService)
    {
        $this->internetService = $internetService;
    
        return $this;
    }

    /**
     * Get internetService
     *
     * @return string 
     */
    public function getInternetService()
    {
        return $this->internetService;
    }

    /**
     * Set internetSpeed
     *
     * @param string $internetSpeed
     * @return SystemMessages
     */
    public function setInternetSpeed($internetSpeed)
    {
        $this->internetSpeed = $internetSpeed;
    
        return $this;
    }

    /**
     * Get internetSpeed
     *
     * @return string 
     */
    public function getInternetSpeed()
    {
        return $this->internetSpeed;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SystemMessages
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set detectedInfo
     *
     * @param string $detectedInfo
     * @return SystemMessages
     */
    public function setDetectedInfo($detectedInfo)
    {
        $this->detectedInfo = $detectedInfo;
    
        return $this;
    }

    /**
     * Get detectedInfo
     *
     * @return string 
     */
    public function getDetectedInfo()
    {
        return $this->detectedInfo;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return SystemMessages
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return SystemMessages
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get isClosed
     *
     * @return integer
     */
    public function getisClosed()
    {
        return $this->isClosed;
    }

    /**
     * Set isClosed
     *
     * @param integer $isClosed
     * @return SystemMessages
     */
    public function setisClosed($isClosed)
    {
        $this->isClosed = $isClosed;

        return $this;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('email', new Email(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('email', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('browser', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('description', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('internetSpeed', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('internetService', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('operationSystem', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('instrument', new NotBlank(array(
            'groups' => array('create'),
        )));
        $metadata->addPropertyConstraint('country', new NotBlank(array(
            'groups' => array('create'),
        )));

    }

}