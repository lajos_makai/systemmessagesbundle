<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MakaiSystemMessagesBundle extends Bundle
{
}
