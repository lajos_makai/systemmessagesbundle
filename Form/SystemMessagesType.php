<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemMessagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email','email', array(
                'label' => 'form.system_message.email'
            ))
            ->add('country', 'text', array(
                'label' => 'form.system_message.country'
            ))
            ->add('instrument', 'text', array(
                'label' => 'form.system_message.instrument'
            ))
            ->add('browser','text', array(
                'label' => 'form.system_message.browser'
            ))
            ->add('operationSystem','text', array(
                'label' => 'form.system_message.operationSystem'
            ))
            ->add('internetService','text', array(
                'label' => 'form.system_message.internetService'
            ))
            ->add('internetSpeed','text', array(
                'label' => 'form.system_message.internetSpeed'
            ))
            ->add('description', 'textarea', array(
                'label' => 'form.system_message.description'
            ))
            ->add('detectedInfo', 'hidden', array(
                'label' => 'form.system_message.detectedInfo'
            ))
            //->add('createdAt')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Makai\SystemMessagesBundle\Entity\SystemMessages',
            'validation_groups' => array('create')
        ));
    }

    public function getName()
    {
        return 'makai_systemmessagesbundle_systemmessagestype';
    }
}
