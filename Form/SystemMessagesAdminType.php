<?php
/*
* This file is part of the MakaiSystemMessagesBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\SystemMessagesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemMessagesAdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment','textarea', array(
                'required'  => false,
                'label' => 'form.system_message.comment',
                'attr' => array('cols' => '80', 'rows' => '5')
            ))
            ->add('isClosed', 'checkbox', array(
                'required'  => false,
                'label' => 'Le van zárva'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Makai\SystemMessagesBundle\Entity\SystemMessages',
            'validation_groups' => array('update')
        ));
    }

    public function getName()
    {
        return 'makai_systemmessagesbundle_systemmessagestype';
    }
}
